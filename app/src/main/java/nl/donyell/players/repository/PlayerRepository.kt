package nl.donyell.players.repository

import android.app.Application
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import nl.donyell.players.R
import nl.donyell.players.model.Player

class PlayerRepository(private val application: Application) {

    suspend fun getPlayers(): List<Player>? {
        val file = application.resources.openRawResource(R.raw.players)
        return withContext(Dispatchers.IO) {
            csvReader()
                .readAll(file)
                .drop(1) // The first line contains the headers
                .map { Player(it) }
                .toList()
        }
    }
}