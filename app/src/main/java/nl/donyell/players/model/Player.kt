package nl.donyell.players.model

import java.text.SimpleDateFormat
import java.util.*

data class Player(val csvRow: List<String>) {
    val id: String = csvRow[0]
    val score: Int = csvRow[1].toInt()
    val firstName: String = csvRow[2]
    val lastName: String = csvRow[3]
    val createdAtString = csvRow[4]

    // Date format: 2018-02-20T10:59:11Z
    val createdAt: Date?

    init {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.getDefault())
        createdAt = dateFormat.parse(createdAtString)
    }
}