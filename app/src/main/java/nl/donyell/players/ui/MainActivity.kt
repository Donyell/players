package nl.donyell.players.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import nl.donyell.players.R
import nl.donyell.players.databinding.ActivityMainBinding
import nl.donyell.players.viewmodel.PlayerViewModel
import nl.donyell.players.viewmodel.PlayerViewModelFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this,
                R.layout.activity_main
            )

        val application = requireNotNull(this).application

        val viewModelFactory =
            PlayerViewModelFactory(application)

        val playerViewModel =
            ViewModelProvider(this, viewModelFactory).get(PlayerViewModel::class.java)

        binding.playerViewModel = playerViewModel
        binding.lifecycleOwner = this

        val adapter = PlayerAdapter()
        binding.rvPlayerList.adapter = adapter
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        binding.rvPlayerList.addItemDecoration(itemDecoration)

        playerViewModel.players.observe(this, Observer { players ->
            adapter.submitList(players)
        })
    }
}
