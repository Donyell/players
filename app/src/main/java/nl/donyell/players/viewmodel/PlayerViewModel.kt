package nl.donyell.players.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import nl.donyell.players.model.Player
import nl.donyell.players.repository.PlayerRepository

const val TAG = "PlayerViewModel"

class PlayerViewModel(application: Application) : AndroidViewModel(application) {

    private var _players = MutableLiveData<List<Player>>()
    val players: LiveData<List<Player>>
        get() = _players

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val playerRepository = PlayerRepository(application)

    init {
        getPlayers()
    }

    private fun getPlayers() = uiScope.launch {
        try {
            _players.value = playerRepository.getPlayers()
        } catch (e: Exception) {
            Log.e(TAG, "e: ${e.localizedMessage}")
        }
    }

    fun onSortDateNewToOldClick() {
        _players.value = players.value?.sortedByDescending { it.createdAt }
    }

    fun onSortDateOldToNewClick() {
        _players.value = players.value?.sortedBy { it.createdAt }
    }

    fun onSortScoreHighToLowClick() {
        _players.value = players.value?.sortedByDescending { it.score }
    }

    fun onSortScoreLowToHighClick() {
        _players.value = players.value?.sortedBy { it.score }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}