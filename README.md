# Assignment

Create an app that is capable of parsing the players.csv contents in the background and display them. (If time allows it add an option to sort the players by score and/or created date).

# Motivation
I was looking for a small project that allowed me to play with [Data Binding](https://developer.android.com/topic/libraries/data-binding/) and [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html).

# Todo
- Different models for the different layers
- Add tests
